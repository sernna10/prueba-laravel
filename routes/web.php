<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('index');
})->name('index');

Route::view('/', 'index')->name('index');

Route::view('/usuario', 'usuario')->name('usuario');

Route::get('/usuario', 'UsuarioController@index')->name('usuario.index');
Route::get('/usuario/{id}', 'UsuarioController@show')->name('usuario.show');
Route::post('usuario', 'UsuarioController@store');

Route::get('/editUsuario', 'UsuarioController@index')->name('usuario.editUsuario');
Route::get('/editUsuario/{usuario}/editar', 'UsuarioController@edit')->name('usuario.edit');
Route::get('editUsuario', 'UsuarioController@store');

Route::get('/crearUsuario/crear', 'UsuarioController@create')->name('usuario.create');
Route::get('/crearUsuario/{id}', 'UsuarioController@showNav')->name('usuario.showNav');
Route::post('crearUsuario', 'UsuarioController@nuevoUsuario')->name('usuario.nuevoUsuario');
Route::patch('/crearUsuario/{usuario}', 'UsuarioController@update')->name('usuario.update');

Route::delete('/usuario/{usuario}', 'UsuarioController@destroy')->name('usuario.destroy');

Auth::routes();