@extends('layout')

@section('titulo', 'Busqueda')

@section('content')
    <h1>LISTA DE USUARIOS</h1>

    {{-- <ul>
        @forelse ($usuario as $itemUsuario)
        <li><a href="{{ route('usuario.show', $itemUsuario) }}">{{ $itemUsuario->correo }}</a></li>
        @empty
        <li>Vacio</li>
        @endforelse
    </ul> --}}

    {{--
    <hr> --}}

    {{-- <div class="col-md-6 col-md-offset-3">
        <form action="{{ route('usuario.index') }}" method="post">
            @csrf
            <form>
                <label for="txtNomUsuario">Nombre usuario</label>
                <div class="form-group">
                    <input type="text" class="form-control" value="{{ old('txtNomUsuario') }}" name="txtNomUsuario"
                        id="txtNomUsuario" placeholder="Ingresar nombre de usuario">
                    {!! $errors->first('txtNomUsuario', '<small>:message</small><br>') !!}
                </div>
                <label for="txtContrasena">Contraseña</label>
                <div class="form-group">
                    <input type="password" class="form-control" value="{{ old('txtContrasena') }}" name="txtContrasena"
                        id="txtContrasena" placeholder="Ingresar contraseña">
                    {!! $errors->first('txtContrasena', '<small>:message</small><br>') !!}
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" type="submit">Cancelar</button>

                    <button class="btn btn-success">Iniciar sesión</button>
                </div>
            </form>
        </form>
    </div> --}}

    {{--
    <hr> --}}

    <div class="container-fluid mt-5">
        <div class="col-md-10 offset-md-1">

            <h1 class="title">TABLA VENDEDORES</h1>

            <div class="form-group">
                <a class="btn btn-success" href="{{ route('usuario.create') }}" title="Ver datos">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-plus" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zM13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z" />
                    </svg>
                </a>
                <label>Agregar nuevo usuario</label>
            </div>

            <table class="table table-responsive table-hover table-dark table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Documento</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Tipo de rol</th>
                        <th scope="col">Fecha de creación</th>
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($usuario as $usu)
                        @if ($usu->idRol == 2)
                            <tr>
                                <th scope="row">{{ $usu->id }}</th>
                                <td>{{ $usu->documento }}</td>
                                <td>{{ $usu->nombre }}</td>
                                <td>{{ $usu->correo }}</td>
                                <td>{{ $usu->direccion }}</td>
                                @if ($usu->idRol == 2)
                                    <td>Vendedor</td>
                                @endif
                                <td>{{ $usu->created_at }}</td>
                                <td>
                                    <a class="btn btn-warning" href="" title="Ver datos">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-list-ol"
                                            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5z" />
                                            <path
                                                d="M1.713 11.865v-.474H2c.217 0 .363-.137.363-.317 0-.185-.158-.31-.361-.31-.223 0-.367.152-.373.31h-.59c.016-.467.373-.787.986-.787.588-.002.954.291.957.703a.595.595 0 0 1-.492.594v.033a.615.615 0 0 1 .569.631c.003.533-.502.8-1.051.8-.656 0-1-.37-1.008-.794h.582c.008.178.186.306.422.309.254 0 .424-.145.422-.35-.002-.195-.155-.348-.414-.348h-.3zm-.004-4.699h-.604v-.035c0-.408.295-.844.958-.844.583 0 .96.326.96.756 0 .389-.257.617-.476.848l-.537.572v.03h1.054V9H1.143v-.395l.957-.99c.138-.142.293-.304.293-.508 0-.18-.147-.32-.342-.32a.33.33 0 0 0-.342.338v.041zM2.564 5h-.635V2.924h-.031l-.598.42v-.567l.629-.443h.635V5z" />
                                        </svg>
                                    </a>
                                    <a class="btn btn-info" href="{{ route('usuario.edit', $usu->id) }}"" title=" Editar">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-wrench"
                                            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M.102 2.223A3.004 3.004 0 0 0 3.78 5.897l6.341 6.252A3.003 3.003 0 0 0 13 16a3 3 0 1 0-.851-5.878L5.897 3.781A3.004 3.004 0 0 0 2.223.1l2.141 2.142L4 4l-1.757.364L.102 2.223zm13.37 9.019L13 11l-.471.242-.529.026-.287.445-.445.287-.026.529L11 13l.242.471.026.529.445.287.287.445.529.026L13 15l.471-.242.529-.026.287-.445.445-.287.026-.529L15 13l-.242-.471-.026-.529-.445-.287-.287-.445-.529-.026z" />
                                        </svg>
                                    </a>
                                    <form action="{{ route('usuario.destroy', $usu->id) }}" method="post">
                                        @csrf @method('DELETE')
                                        <button class="btn btn-danger" title="Eliminar">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash"
                                                fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                                <path fill-rule="evenodd"
                                                    d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endif

                    @empty
                        <p>No hay nada que mostrar.</p>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <hr>

    <div class="container-fluid mt-5">
        <div class="col-md-10 offset-md-1">

            <h1 class="title">TABLA CLIENTES</h1>

            <div class="form-group">
                <a class="btn btn-success" href="{{ route('usuario.create') }}" title="Ver datos">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-plus" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zM13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z" />
                    </svg>
                </a>
                <label>Agregar nuevo usuario</label>
            </div>

            <table class="table table-responsive table-hover table-dark table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Documento</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Tipo de rol</th>
                        <th scope="col">Fecha de creación</th>
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($usuario as $usu)
                        @if ($usu->idRol == 3)
                            <tr>
                                <th scope="row">{{ $usu->id }}</th>
                                <td>{{ $usu->documento }}</td>
                                <td>{{ $usu->nombre }}</td>
                                <td>{{ $usu->correo }}</td>
                                <td>{{ $usu->direccion }}</td>
                                @if ($usu->idRol == 3)
                                    <td>Cliente</td>
                                @endif
                                <td>{{ $usu->created_at }}</td>
                                <td>
                                    <a class="btn btn-warning" href="" title="Ver datos">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-list-ol"
                                            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5z" />
                                            <path
                                                d="M1.713 11.865v-.474H2c.217 0 .363-.137.363-.317 0-.185-.158-.31-.361-.31-.223 0-.367.152-.373.31h-.59c.016-.467.373-.787.986-.787.588-.002.954.291.957.703a.595.595 0 0 1-.492.594v.033a.615.615 0 0 1 .569.631c.003.533-.502.8-1.051.8-.656 0-1-.37-1.008-.794h.582c.008.178.186.306.422.309.254 0 .424-.145.422-.35-.002-.195-.155-.348-.414-.348h-.3zm-.004-4.699h-.604v-.035c0-.408.295-.844.958-.844.583 0 .96.326.96.756 0 .389-.257.617-.476.848l-.537.572v.03h1.054V9H1.143v-.395l.957-.99c.138-.142.293-.304.293-.508 0-.18-.147-.32-.342-.32a.33.33 0 0 0-.342.338v.041zM2.564 5h-.635V2.924h-.031l-.598.42v-.567l.629-.443h.635V5z" />
                                        </svg>
                                    </a>
                                    <a class="btn btn-info" href="{{ route('usuario.edit', $usu->id) }}" title="Editar">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-wrench"
                                            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M.102 2.223A3.004 3.004 0 0 0 3.78 5.897l6.341 6.252A3.003 3.003 0 0 0 13 16a3 3 0 1 0-.851-5.878L5.897 3.781A3.004 3.004 0 0 0 2.223.1l2.141 2.142L4 4l-1.757.364L.102 2.223zm13.37 9.019L13 11l-.471.242-.529.026-.287.445-.445.287-.026.529L11 13l.242.471.026.529.445.287.287.445.529.026L13 15l.471-.242.529-.026.287-.445.445-.287.026-.529L15 13l-.242-.471-.026-.529-.445-.287-.287-.445-.529-.026z" />
                                        </svg>
                                    </a>
                                    <form action="{{ route('usuario.destroy', $usu->id) }}" method="post">
                                        @csrf @method('DELETE')
                                        <button class="btn btn-danger" title="Eliminar">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash"
                                                fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                                <path fill-rule="evenodd"
                                                    d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endif

                    @empty
                        <p>No hay nada que mostrar.</p>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>


@endsection
