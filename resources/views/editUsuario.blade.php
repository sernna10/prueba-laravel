@extends('layout')

@section('titulo', 'Editar usuario')

@section('content')

    <div class="container col-md-6 offset-md-3">
        <h1>EDITAR USUARIO</h1>

        <form method="POST" action="{{ route('usuario.update', $editUsuario) }}">
            @csrf
            @method('PATCH')
            <div class="form-group row">
                <label for="inputDocumento" class="col-sm-2 col-form-label">Documento</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputDocumento" name="inputDocumento"
                        value="{{ $editUsuario->documento }}">
                    {!! $errors->first('inputDocumento', '<small>:message</small><br>') !!}
                </div>
            </div>
            <div class="form-group row">
                <label for="inputNombre" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputNombre" name="inputNombre"
                        value="{{ $editUsuario->nombre }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEmail" class="col-sm-2 col-form-label">Correo</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail" name="inputEmail"
                        value="{{ $editUsuario->correo }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputDireccion" class="col-sm-2 col-form-label">Dirección</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputDireccion" name="inputDireccion"
                        value="{{ $editUsuario->direccion }}">
                </div>
            </div>
            <div class="form-group pull-right">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                    <a class="btn btn-danger" href="{{ route('usuario.index') }}">Cancelar</a>
                </div>
            </div>
        </form>
    </div>

@endsection
