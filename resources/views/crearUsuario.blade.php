@extends('layout')

@section('titulo', 'Crear usuario')

@section('content')

    <div class="container col-md-6 offset-md-3">
        <h1>CREAR USUARIO</h1>
        <form method="POST" action="{{ route('usuario.nuevoUsuario') }}">
            @csrf
            <div class="form-group row">
                <label for="inputDocumento" class="col-sm-2 col-form-label">Documento</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputDocumento" name="inputDocumento"
                        value="{{ old('inputDocumento') }}">
                    {!! $errors->first('inputDocumento', '<small>:message</small><br>') !!}
                </div>
            </div>
            <div class="form-group row">
                <label for="inputNombre" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputNombre" name="inputNombre"
                        value="{{ old('inputNombre') }}">
                    {!! $errors->first('inputNombre', '<small>:message</small><br>') !!}
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEmail" class="col-sm-2 col-form-label">Correo</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail" name="inputEmail"
                        value="{{ old('inputEmail') }}">
                    {!! $errors->first('inputEmail', '<small>:message</small><br>') !!}
                </div>
            </div>
            <div class="form-group row">
                <label for="inputContrasena" class="col-sm-2 col-form-label">Contraseña</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputContrasena" name="inputContrasena"
                        value="{{ old('inputContrasena') }}">
                    {!! $errors->first('inputContrasena', '<small>:message</small><br>') !!}
                </div>
            </div>
            <div class="form-group row">
                <label for="inputDireccion" class="col-sm-2 col-form-label">Dirección</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputDireccion" name="inputDireccion"
                        value="{{ old('inputDireccion') }}">
                    {!! $errors->first('inputDireccion', '<small>:message</small><br>') !!}
                </div>
            </div>
            <div class="form-group row col-md-4">
                <label for="inputRol">Rol</label>
                <select id="inputRol" class="form-control"  name="inputRol" value="{{ old('inputRol') }}">
                    <option selected>Vendedor</option>
                    <option>Cliente</option>
                </select>
                {!! $errors->first('inputRol', '<small>:message</small><br>') !!}
            </div>
            <div class="form-group pull-right">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Crear</button>
                    <a class="btn btn-danger" href="{{ route('usuario.index') }}">Cancelar</a>
                </div>
            </div>
        </form>
    </div>

@endsection
