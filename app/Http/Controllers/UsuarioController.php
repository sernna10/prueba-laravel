<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = Usuario::get();

        return view('usuario', compact('usuario'));
    }

    public function showNav($id)
    {
        return view('usuario', [
            'usuario' => Usuario::findOrFail($id)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crearUsuario');
    }

    public function nuevoUsuario()
    {

        $inputRol = request('inputRol');

        $idRol = null;

        if ($inputRol == 'Vendedor') {
            
            $idRol = 2;

        } elseif ($inputRol == 'Cliente') {
            
            $idRol = 3;

        } else {
            
            $idRol = null;
            
        }

        Usuario::create([
        'documento' => request('inputDocumento'),
        'nombre' => request('inputNombre'),
        'correo' => request('inputEmail'),
        'contrasena' => request('inputContrasena'),
        'direccion' => request('inputDireccion'),
        'idRol' => $idRol
        ]);

        return redirect()->route('usuario.index');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'txtNomUsuario' => 'required',
            'txtContrasena' => [
                'required',
                'numeric'
            ]
        ]);

        return 'Datos validados.';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('usuario', [
            'usuario' => Usuario::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Usuario $usuario)
    {
        return view('editUsuario', [
            'editUsuario' => $usuario
        ]);
    }

    // public function editUsuario($id)
    // {
    //     $editUsuario = Usuario::get();

    //     return view('editUsuario', compact('editUsuario'));
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Usuario $usuario)
    {
        $usuario->update([
            'documento' => request('inputDocumento'),
            'nombre' => request('inputNombre'),
            'correo' => request('inputEmail'),
            'direccion' => request('inputDireccion')
        ]);

        return redirect()->route('usuario.index', $usuario);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usuario $usuario)
    {
        $usuario->delete();

        return redirect()->route('usuario.index');
    }
}
