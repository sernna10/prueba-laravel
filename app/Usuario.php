<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $fillable = ['documento', 'nombre', 'correo', 'contrasena', 'direccion', 'idRol'];
    protected $table = 'tblUsuario';
}
